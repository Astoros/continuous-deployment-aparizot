FROM composer:latest AS composer

RUN mkdir /code

COPY composer.json /code
COPY composer.lock /code

WORKDIR /code

RUN composer install --no-dev --no-scripts --classmap-authoritative --ignore-platform-reqs --prefer-dist


FROM php:7.2-apache

RUN docker-php-ext-install pdo pdo_mysql \
 && a2enmod rewrite \
 && sed -i 's!/var/www/html!/var/www/html/public!g' /etc/apache2/sites-available/*.conf

COPY . /var/www/html
COPY --from=composer /code/vendor /var/www/html/vendor

WORKDIR /var/www/html